
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Main {
	private LinkedBlockingQueue<String> queueIn, queueOut;
	private final String fileName = "textIn.txt";
	private static final String End_FLAG = UUID.randomUUID().toString();

	public static void main(String[] args) {
		try {
			TimeView.startTime();
			new Main().start();
			System.out.println("Main end");
			TimeView.endTime();
		} catch (InterruptedException e) {
			System.err.println("Can not run the app");
			e.printStackTrace();
		}
	}

	public Main() {
		queueIn = new LinkedBlockingQueue<>();
		queueOut = new LinkedBlockingQueue<>();
	}

	public void start() throws InterruptedException {
		textGen();
		ExecutorService executors = Executors.newFixedThreadPool(3);
		executors.submit(new ReadTask(queueIn, fileName, End_FLAG));
		executors.submit(new ChangeTask(queueIn, queueOut, End_FLAG));
		executors.submit(new WriteTask(queueOut, End_FLAG));
		executors.shutdown();
		executors.awaitTermination(1, TimeUnit.MINUTES);
	}

	private void textGen() {
		new TextGen(fileName).createText();
	}
}
