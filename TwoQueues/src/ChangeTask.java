import java.util.concurrent.LinkedBlockingQueue;

public class ChangeTask implements Runnable {
	private LinkedBlockingQueue<String> queueIn, queueOut;
	private final static String RIGHT = "---------ПЕЧЕНЬЕ---------";
	private final static String WRONG = "М_А_Р_М_Е_Л_А_Д";
	private final static String SPLITTER = ",";
	private String endOfFile;

	public ChangeTask(LinkedBlockingQueue<String> queueIn, LinkedBlockingQueue<String> queueOut, String endOfFile) {
		this.queueIn = queueIn;
		this.queueOut = queueOut;
		this.endOfFile = endOfFile;
	}

	@Override
	public void run() {
		try {
			changeText();
		} catch (InterruptedException e) {
			System.err.println("Can not add string into queueOut and remove string from queueIn");
			e.printStackTrace();
		}
		System.out.println("Change end");
	}

	private void changeText() throws InterruptedException {
		boolean isFullQueue = true;
		while (isFullQueue) {
			String line = queueIn.take();
			if (!line.equals(endOfFile)) {
				String[] arraySplit = line.split(SPLITTER);
				if (arraySplit[1].equals(WRONG)) {
					queueOut.put(arraySplit[0] + SPLITTER + RIGHT);
				} else {
					queueOut.put(line);
				}
			} else {
				isFullQueue = false;
				queueOut.put(endOfFile);
			}
		}
	}
}
