import java.util.Date;

public class TimeView {
	private static long begin, end;

	static void startTime() {
		begin = new Date().getTime();
	}

	static void endTime() {
		end = new Date().getTime();
		long time = end - begin;
		System.out.println("Total time " + time + " ms");
	}
}
