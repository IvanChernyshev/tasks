import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public class ReadTask implements Runnable {
	private LinkedBlockingQueue<String> queueIn;
	private String fileName;
	private String endOfFile;

	public ReadTask(LinkedBlockingQueue<String> queue, String fileName, String endOfFile) {
		this.queueIn = queue;
		this.fileName = fileName;
		this.endOfFile = endOfFile;
	}

	@Override
	public void run() {
		try {
			readText();
		} catch (InterruptedException e) {
			System.err.println("Can not add string into queueIn");
			e.printStackTrace();
		}
		System.out.println("Read end");
	}

	private void readText() throws InterruptedException {
		String line;
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)))) {
			while ((line = reader.readLine()) != null) {
				queueIn.put(line);
			}
			queueIn.put(endOfFile);
		} catch (FileNotFoundException e) {
			System.err.println("Can not instantiate BufferedReader");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Can not read " + fileName);
			e.printStackTrace();
		}
	}
}
