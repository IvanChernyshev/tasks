import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public class WriteTask implements Runnable {
	private LinkedBlockingQueue<String> queueOut;
	private final static String OUT_FILE_NAME = "textOut.txt";
	private String endOfFile;

	public WriteTask(LinkedBlockingQueue<String> queue, String endOfFile) {
		this.queueOut = queue;
		this.endOfFile = endOfFile;
	}

	@Override
	public void run() {
		try {
			writeText();
			System.out.println("Write end");
		} catch (InterruptedException e) {
			System.err.println("Can not remove string from queueOut");
			e.printStackTrace();
		}
	}

	private void writeText() throws InterruptedException {
		boolean queueIsFull = true;
		String line;
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(OUT_FILE_NAME)))) {
			while (queueIsFull) {
				line = queueOut.take();
				if (line != endOfFile) {
					writer.write(line);
					writer.newLine();
				} else {
					queueIsFull = false;
				}
			}
		} catch (IOException e) {
			System.err.println("Can not instantiate BufferedWriter");
			e.printStackTrace();
		}
	}
}
