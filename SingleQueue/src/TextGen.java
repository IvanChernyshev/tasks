import java.io.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class TextGen {
	private static final int NUMBER_OF_ROWS = 100;
	private String[] words = { "варенье", "джем", "повидло", "М_А_Р_М_Е_Л_А_Д", "зефир", "пастила", "карамель", "суфле",
			"шоколад", "марципан", "халва", "мороженное" };
	private String fileName;

	public TextGen(String fileName) {
		this.fileName = fileName;
	}

	public void createText() {
		writeText(getRandomNumber());
	}

	private void writeText(long randomNum) {
		Random random = new Random();
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileName)))) {
			for (int i = 0; i < NUMBER_OF_ROWS; i++) {
				writer.write(randomNum + i + "," + words[random.nextInt(words.length)]);
				writer.newLine();
			}
		} catch (IOException e) {
			System.err.println("Can not read file " + fileName);
			e.printStackTrace();
		}
	}

	private long getRandomNumber() {
		return ThreadLocalRandom.current().nextLong(10000, NUMBER_OF_ROWS * 10000);
	}
}
