import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;

public class ReadTask implements Runnable {
	private LinkedBlockingQueue<MyData> queueIn;
	private String fileName;
	private Lock lock;

	public ReadTask(LinkedBlockingQueue<MyData> queue, String fileName, Lock lock) {
		this.queueIn = queue;
		this.fileName = fileName;
		this.lock = lock;
	}

	@Override
	public void run() {
		try {
			readText();
		} catch (InterruptedException e) {
			System.err.println("Can not add string into queueIn");
			e.printStackTrace();
		}
		System.out.println("Read end");
	}

	private void readText() throws InterruptedException {
		String line;
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)))) {
			while ((line = reader.readLine()) != null) {
				lock.lock();
				try {
					queueIn.put(new MyData(line));
				} finally {
					lock.unlock();
				}
			}
			lock.lock();
			try {
				queueIn.put(new MyData(true));
			} finally {
				lock.unlock();
			}
		} catch (FileNotFoundException e) {
			System.err.println("Can not instantiate BufferedReader");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Can not read " + fileName);
			e.printStackTrace();
		}
	}
}
