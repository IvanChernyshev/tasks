import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class ChangeTask implements Runnable {
	private final static String RIGHT = "---------ПЕЧЕНЬЕ---------";
	private final static String WRONG = "М_А_Р_М_Е_Л_А_Д";
	private LinkedBlockingQueue<MyData> queueIn;
	private Lock lock;
	private Condition newHeadAvailable, newHandledAvailable;
	private boolean queueIsFull;

	public ChangeTask(LinkedBlockingQueue<MyData> queueIn, Lock lock, Condition newHeadAvailable,
			Condition newHandledAvailable) {
		this.queueIn = queueIn;
		this.lock = lock;
		this.newHeadAvailable = newHeadAvailable;
		this.newHandledAvailable = newHandledAvailable;
	}

	@Override
	public void run() {
		try {
			changeText();
		} catch (InterruptedException e) {
			System.err.println("Can not add string into queueOut and remove string from queueIn");
			e.printStackTrace();
		}
		System.out.println("Change end");
	}

	private void changeText() throws InterruptedException {
		queueIsFull = true;
		while (queueIsFull) {
			lock.lock();
			try {
				MyData data = queueIn.peek();
				if (data == null) {
					continue;
				}
				updateData(data);
			} finally {
				lock.unlock();
			}
		}
	}

	private void updateData(MyData data) throws InterruptedException {
		if (!data.isLast()) {
			if (!data.isChanged()) {
				data.setName(data.getName().replaceAll(WRONG,  RIGHT));
				data.setChanged(true);
				newHandledAvailable.signal();
			} else {
				newHeadAvailable.await();
			}
		} else {
			queueIsFull = false;
		}
	}
}
