
public class MyData {
	private String id, name;
	private boolean isChanged, isLast;

	public MyData(String description) {
		init(description);
	}

	public MyData(boolean isLast) {
		this.isLast = isLast;
	}

	private void init(String description) {
		String[] mass = description.split(",");
		this.id = mass[0];
		this.name = mass[1];
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isChanged() {
		return isChanged;
	}

	public void setChanged(boolean isChanged) {
		this.isChanged = isChanged;
	}

	@Override
	public String toString() {
		return id + "," + name;
	}

	public boolean isLast() {
		return isLast;
	}

	public void setLast(boolean isLast) {
		this.isLast = isLast;
	}
}
