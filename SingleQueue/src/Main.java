
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
	private LinkedBlockingQueue<MyData> queueIn;
	private final String fileName = "textIn.txt";

	public static void main(String[] args) {
		try {
			TimeView.startTime();
			new Main().start();
			System.out.println("Main end");
			TimeView.endTime();
		} catch (InterruptedException e) {
			System.err.println("Can not run the app");
			e.printStackTrace();
		}
	}

	public Main() {
		queueIn = new LinkedBlockingQueue<>();
	}

	public void start() throws InterruptedException {
		textGen();
		Lock lock = new ReentrantLock();
		Condition newHeadAvailable = lock.newCondition();
		Condition newHandledAvailable = lock.newCondition();
		ExecutorService executors = Executors.newFixedThreadPool(3);
		executors.submit(new ReadTask(queueIn, fileName, lock));
		executors.submit(new ChangeTask(queueIn, lock, newHeadAvailable, newHandledAvailable));
		executors.submit(new WriteTask(queueIn, lock, newHeadAvailable, newHandledAvailable));
		executors.shutdown();
		executors.awaitTermination(1, TimeUnit.MINUTES);
	}

	private void textGen() {
		new TextGen(fileName).createText();
	}
}
