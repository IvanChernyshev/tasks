import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class WriteTask implements Runnable {
	private final static String OUT_FILE_NAME = "textOut.txt";
	private final static String SPLITTER = ",";
	private LinkedBlockingQueue<MyData> queueOut;
	private Lock lock;
	private Condition newHeadAvailable, newHandledAvailable;
	private boolean queueIsFull;

	public WriteTask(LinkedBlockingQueue<MyData> queue, Lock lock, Condition newHeadAvailable,
			Condition newHandledAvailable) {
		this.queueOut = queue;
		this.lock = lock;
		this.newHeadAvailable = newHeadAvailable;
		this.newHandledAvailable = newHandledAvailable;
	}

	@Override
	public void run() {
		try {
			writeText();
			System.out.println("Write end");
		} catch (InterruptedException e) {
			System.err.println("Can not remove string from queueOut");
			e.printStackTrace();
		}
	}

	private void writeText() throws InterruptedException {
		queueIsFull = true;
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(OUT_FILE_NAME)))) {
			while (queueIsFull) {
				lock.lock();
				try {
					MyData data = queueOut.peek();
					if (data == null) {
						continue;
					}
					tryToWrite(data, writer);
				} finally {
					lock.unlock();
				}
			}
		} catch (IOException e) {
			System.err.println("Can not instantiate BufferedWriter");
			e.printStackTrace();
		}
	}

	private void tryToWrite(MyData data, BufferedWriter writer) throws InterruptedException, IOException {
		if (!data.isLast()) {
			if (data.isChanged()) {
				data = queueOut.take();
				writer.write(data.getId() + SPLITTER + data.getName());
				writer.newLine();
				newHeadAvailable.signal();
			} else {
				newHandledAvailable.await();
			}
		} else {
			queueIsFull = false;
		}
	}
}
